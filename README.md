# Curriculum Vitae

Link to resume:

- https://millernb.gitlab.io/curriculum-vitae/cv.pdf

To update cv, edit `details.yaml`, execute `python yaml_to_tex.py`, and recompile `cv.tex`.