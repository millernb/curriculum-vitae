#!/bin/python
# coding: utf-8

import yaml

filepath = './details.yaml'
with open(filepath) as file:
    yaml_content = yaml.safe_load(file)

output_text = r'''
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\documentclass[11pt, a4paper]{awesome-cv}
\geometry{left=1.4cm, top=.8cm, right=1.4cm, bottom=1.8cm, footskip=.5cm}

\fontdir[fonts/]
\colorlet{awesome}{awesome-darknight}


\setbool{acvSectionColorHighlight}{true}
\renewcommand{\acvHeaderSocialSep}{\quad\textbar\quad}

'''

first_name, last_name = '', ''
if 'first_name' in yaml_content['about']:
    first_name = yaml_content['about']['first_name']
if 'last_name' in yaml_content['about']:
    last_name = yaml_content['about']['last_name']


output_text += r'\name{%s}{%s}'%(first_name, last_name) +'\n'
for key in yaml_content['about']:
    if key not in ['first_name', 'last_name']:
        output_text += r'\%s{%s}'%(key, yaml_content['about'][key]) + '\n'


output_text += r'''

\begin{document}

\makecvheader[C]

\makecvfooter
{\today}
{%s %s~~~·~~~Curriculum vitae}
{\thepage}
'''%(first_name, last_name)

for section in yaml_content:
    output_text += '\n'
    if section != 'about':
        output_text += r'''
\cvsection{%s}
'''%(section)
        for temp_array in yaml_content[section]:

            if len(temp_array) < 4:
                for k in range(4 - len(temp_array)):
                    temp_array.append('')

            output_text += r'''
\cventry
{%s}
{%s}
{%s}
{%s}'''%tuple(temp_array[:4])

            if len(temp_array) > 4:
                output_text += r'''
{
\begin{cvitems}'''

                for item in temp_array[4:]:
                    output_text += r'''
\item {%s}'''%(item)


                output_text += r'''
\end{cvitems}
}'''

            else:
                output_text += r'''
{}'''

output_text += r'''

\end{document}'''

with open('cv.tex', 'w') as text_file:
    text_file.write(output_text)